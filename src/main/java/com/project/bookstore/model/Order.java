package com.project.bookstore.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "Orders")
@JsonIgnoreProperties(
    value = {"id", "deleted"},
    allowSetters = true
)
public class Order implements Serializable  {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)

    @Column(name="id", nullable=false)
    private Long id;

    @Column(name="userid", nullable=false)
    private Long userid;

    @Column(name="total", nullable=false)
    private Integer total;

    @Temporal(TemporalType.DATE)
    @Column(name="orderdate", nullable=false)
    private Date orderdate;

    @Column(name="deleted", nullable=false)
    private Boolean deleted;

    @OneToMany
    @JoinColumn(name = "orderid")    
    private List<OrderDetail> orderdetails = new ArrayList<>();
  
    // private List<Long> bookids = new ArrayList<>();

    public Order(){}

    public Order(Long id, Long userid, Long orderid, Date orderdate, Integer total) {
        this.id = id;
        this.userid = userid;
        this.orderdate = orderdate;
        this.total = total;
        this.deleted = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }


    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @JsonProperty("order_date")
    public Date getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(Date orderdate) {
        this.orderdate = orderdate;
    }


    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean id) {
        // need query
        this.deleted = true;
    }

    // public  List<Long> getBookids() {
    //     return bookids;
    // }

    // public void setBookids(List<Long> bookids) {
    //     this.bookids = bookids;
    // }

    public List<OrderDetail> getOrderDetails() {
        return  orderdetails;
    }

    public void setOrderDetails(List<OrderDetail> orderdetails) {
        this.orderdetails = orderdetails;
    }

}