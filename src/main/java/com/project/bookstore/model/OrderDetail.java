package com.project.bookstore.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "Orderdetails")
@JsonIgnoreProperties(
    value = {"id", "deleted"},
    allowSetters = true
)
public class OrderDetail implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name="orderid", nullable=false)
    private Long orderid;

    @Column(name="bookid", nullable=false)
    private Long bookid;

    @JsonProperty("book_name")
    @Column(name="bookname", nullable=false)
    private String bookname;

    @Column(name="price", nullable=false)
    private BigDecimal price;

    @Column(name="count", nullable=false)
    private Integer count;

    @Column(name="deleted", nullable=false)
    private Boolean deleted;

    public OrderDetail(){}

    public OrderDetail(Long id, Long orderid, Long bookid, String bookname, BigDecimal price, Integer count) {
        this.id = id;
        this.orderid = orderid;
        this.bookid = bookid;
        this.bookname = bookname;
        this.price = price;
        this.count = count;
        this.deleted = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getOrderId() {
        return orderid;
    }

    public void setOrderId(Long orderid) {
        this.orderid = orderid;
    }


    public Long getBookId() {
        return bookid;
    }

    public void setBookId(Long bookid) {
        this.bookid = bookid;
    }


    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean id) {
        this.deleted = true;
    }
}