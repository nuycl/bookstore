package com.project.bookstore.model;

/******* BASE ******/
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
/******* JSON ******/
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Book implements Serializable {
    private Long id;
    private BigDecimal price;
    private String author_name;
    private String book_name;
    private Boolean is_recommended;

    public Book() {

    }
    public Book(Long id, String author_name, String book_name, BigDecimal price, Boolean recommended) {
        this.id = id;
        this.author_name = author_name;
        this.book_name = book_name;
        this.price = price;
        this.is_recommended = is_recommended;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) { this.price = price; }


    @JsonProperty("author")
    public String getAuthorName() {
        return author_name;
    }

    public void setAuthorName(String authorName) {
        this.author_name = authorName;
    }

    @JsonProperty("book")
    public String getBookName() {
        return book_name;
    }

    public void setBookName(String bookName) {
        this.book_name = bookName;
    }

    @JsonIgnore
    public Boolean getRecomemded() {
       return is_recommended;
    }

    public void setRecomemded(Boolean flag) {
        this.is_recommended = flag;
    }


    public static Comparator<Book> BookNameComparator = new Comparator<Book>() {

        public int compare(Book n1, Book n2) {
            String name1 = n1.getBookName().toUpperCase();
            String name2 = n2.getBookName().toUpperCase();
            return name1.compareTo(name2);
        }
    };
}