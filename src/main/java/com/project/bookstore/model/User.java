package com.project.bookstore.model;

/******* BASE ******/
import java.util.Date;
import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
/******* JSON ******/
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
@Table(name = "Users")
@JsonIgnoreProperties(
    value = {"id", "password", "deleted", "username"},
    allowSetters = true
)
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    @Column(name="name", nullable=false)
    @NotEmpty(message = "*Please provide your name")
    private String name;

    @Column(name="surname", nullable=false)
    @NotEmpty(message = "*Please provide your surname")
    private String surname;
    
    @Column(name="date_of_birth", nullable=false)
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date date_of_birth;

    @Column(name="password", nullable=false)
    @Length(min = 4, message = "*Your password must have at least 4 characters")
    @NotEmpty(message = "*Please provide your password")
    private String password;

    @Column(name="deleted")
    private Boolean deleted;

    @OneToMany
    @JoinColumn(name = "userid")    
    private List<Order> orders = new ArrayList<>();

    public User(){}

    public User(Long id, String name, String surname, Date dateOfBirth, String password, Boolean isDeleted) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.date_of_birth = dateOfBirth;
        this.password = password;
        this.deleted = isDeleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @JsonProperty("date_of_birth")
    public Date getDateOfBirth() {
        return date_of_birth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.date_of_birth = dateOfBirth;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

     @JsonProperty("username")
     public String getUserName() {
         return (name + "." + surname);
     }

    public  List<Order> getOrders() {
        return  orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

}