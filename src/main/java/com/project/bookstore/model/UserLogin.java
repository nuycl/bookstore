package com.project.bookstore.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserLogin {
    private String username;
    private String password;

    @JsonProperty("username")
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}