package com.project.bookstore.dao;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
/******* BASE ******/
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/******* BOOKSTORE ******/
import com.project.bookstore.model.User;

@Repository
public class UserDAO implements IUserDAO {
    @PersistenceContext
    EntityManager entityManager;

    public UserDAO(EntityManager manager) {
        this.entityManager = manager;
    }

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public List<User> getAllUsers() {
        List<User> users = entityManager.createQuery("select p from User p", User.class).getResultList();
        return users;
    }

    @Override
    public User getUserById(long id) {
        String qry = "Select u FROM User as u where deleted=false and u.id=(:id)";
        List<User> users = entityManager.createQuery(qry, User.class).setParameter("id",id).getResultList();
        return (users.size() > 0 ? users.get(0) : null);
    }

     @Override
     @Transactional
     public User addUser(User user) {
        if (!userExists(user.getName(), user.getSurname())) {
            String password = user.getPassword();
            user.setPassword(bCryptPasswordEncoder.encode(password));
            user.setDeleted(false);
            entityManager.persist(user);

            if (user.getId() > 0) {
                return user;
            }
         }
            
         return null;
     }

     @Override
     @Transactional
     public User updateUser(User user) {
         User matchedUser = getUserById(user.getId());
         if (matchedUser != null) {
            Date dateOfBirth = user.getDateOfBirth();
            String encrypPW = bCryptPasswordEncoder.encode(user.getPassword());
            matchedUser.setName(user.getName());
            matchedUser.setSurname(user.getSurname());
            matchedUser.setDateOfBirth(dateOfBirth);
            matchedUser.setPassword(encrypPW);
            entityManager.flush();
            
            System.err.println(matchedUser);
            return matchedUser;
         } else {
             return null;
         }
     }

     @Override
     @Transactional
     public boolean deleteUser(long id) {
         User matchedUser = getUserById(id);
         if (matchedUser != null) {
             matchedUser.setDeleted(true);
             entityManager.flush();
             return true;
         } else {
             return false;
         }
     }

    @Override
    public boolean userExists(String name, String surname) {
        String qry = "FROM User u WHERE (u.name =(:name) and u.surname = (:surname))";
        int count = entityManager.createQuery(qry)
                        .setParameter("name", name)
                        .setParameter("surname", surname)
                        .getResultList().size();
        return count > 0 ? true : false;
    }

    @Override
    public User getUserLogin(String username, String password) {
        String[] names = username.split("\\.");
        if (names.length != 2) return null;
        String encrypPW = bCryptPasswordEncoder.encode(password);

        // Note: should use CONCAT(name,'.',surname in query instead)
        String qry = "FROM User u WHERE deleted=false and (u.name = (:name) and u.surname =(:surname))";
        List<User> users = (List<User>) entityManager.createQuery(qry)
                        .setParameter("name", names[0])
                        .setParameter("surname",names[1])
                        .getResultList(); 
        if (users != null && users.size() > 0) {
            Boolean matched = bCryptPasswordEncoder.matches(password, encrypPW);
            if (matched) {
                return users.get(0);
            }
        } 
        return null;
    }
}