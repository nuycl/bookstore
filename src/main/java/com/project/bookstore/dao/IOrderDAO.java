package com.project.bookstore.dao;
import com.project.bookstore.model.Order;
import java.util.List;

public interface IOrderDAO {
    List<Order> getAllOrders();
    Order getOrderById(long id);
    Order addOrder(Order order);
    boolean deleteOrder(long id);

    List<Order> getOrderByUserId(long userid);
    boolean deleteUserOrder(long userid);
}
