package com.project.bookstore.dao;

import java.util.List;
import com.project.bookstore.model.User;

public interface IUserDAO {
    List<User> getAllUsers();
    User getUserById(long id);
    User addUser(User user);
    User updateUser(User user);
    boolean deleteUser(long id);
    boolean userExists(String name, String surname);
    User getUserLogin(String username, String password);
}
