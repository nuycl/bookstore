package com.project.bookstore.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.project.bookstore.model.OrderDetail;

@Repository
public class OrderDetailDAO implements IOrderDetailDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Long> getBookidByOrderIds(List<Long> orderid) {
        String qry = "Select bookid FROM OrderDetail ord WHERE ord.orderid in (:orderid)";
        List<Long> bookids = entityManager.createQuery(qry)
                .setParameter("orderid", orderid)
                .getResultList();
        return bookids;
    }

    @Override
    public List<OrderDetail> getOrderDetailByOrderId(Long orderid) {
        String qry = "Select ord FROM OrderDetail ord WHERE ord.orderid = (:orderid)";
        List<OrderDetail> orderdetails = entityManager.createQuery(qry)
                .setParameter("orderid", orderid)
                .getResultList();
        return orderdetails;
    }

    @Override
    public OrderDetail getOrderDetailById(long id) {
        OrderDetail orderdetail = entityManager.find(OrderDetail.class, id);
        return orderdetail;
    }

     @Override
     @Transactional
     public OrderDetail addOrderDetail(OrderDetail orderdetail) {
         entityManager.persist(orderdetail);
         return orderdetail;
     }

     @Override
     @Transactional
     public boolean deleteOrderDetail(long id) {
         OrderDetail matchedOrderDetail = getOrderDetailById(id);
         if (matchedOrderDetail != null) {
            matchedOrderDetail.setDeleted(true);
            entityManager.flush();
            return true;
         } else {
            return false;
         }

    }

    @Override
    @Transactional
    public boolean deleteOrderDetailByOrderIds(List<Long> orderid) {
        Query query = entityManager.createQuery( "Update OrderDetail SET deleted=true WHERE orderid in (:orderid)");
        int count = query.setParameter("orderid", orderid).executeUpdate();
        return (count > 0);
    }
}