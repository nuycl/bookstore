package com.project.bookstore.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.project.bookstore.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.project.bookstore.model.Order;

@Repository
public class OrderDAO implements IOrderDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private IOrderDetailDAO orderDetailDAO;

    @Override
    public List<Order> getAllOrders() {
        String qry = "FROM Order ord ORDER BY ord.id";
        List<Order> orders = (List<Order>) entityManager.createQuery(qry).getResultList();
        return orders;
    }

    @Override
    public Order getOrderById(long id) {
        String qry = "Select ord FROM Order as ord where deleted=false and ord.id=(:id)";
        List<Order> orders = entityManager.createQuery(qry, Order.class).setParameter("id",id).getResultList();
        return (orders.size() > 0 ? orders.get(0)  : null);
    }

     @Override
     @Transactional
     public Order addOrder(Order order) {
         entityManager.persist(order);
         return order;
     }

     @Override
     @Transactional
     public boolean deleteOrder(long id) {
         Order matchedOrder = getOrderById(id);
         if (matchedOrder != null) {
             matchedOrder.setDeleted(true);
             entityManager.flush();
             return true;
         } else {
             return false;
         }
    }


    //== User Order ===
    @Override
    public List<Order> getOrderByUserId(long userid) {
        String qry = "Select ord FROM Order ord where userid=(:userid)";
        List<Order> orders = entityManager.createQuery(qry, Order.class)
                .setParameter("userid",userid)
                .getResultList();

        return orders;
    }

    @Override
    @Transactional
    public boolean deleteUserOrder(long userid) {
        List<Order> orders = getOrderByUserId(userid);
        try {
            List<Long> orderid = new ArrayList<Long>();
            for (Order ord : orders) {
                ord.setDeleted(true);
                entityManager.flush();
                orderid.add(ord.getId());
            };
            orderDetailDAO.deleteOrderDetailByOrderIds(orderid);
            return  true;
         }
         catch(Exception ex) {
             System.out.println(ex);
             return false;
         }
    }
}