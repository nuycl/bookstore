package com.project.bookstore.dao;
import com.project.bookstore.model.OrderDetail;
import java.util.List;

public interface IOrderDetailDAO {
    List<Long> getBookidByOrderIds(List<Long> orderid);
    List<OrderDetail> getOrderDetailByOrderId(Long orderid);
    OrderDetail getOrderDetailById(long id);
    OrderDetail addOrderDetail(OrderDetail orderdetail);
    boolean deleteOrderDetail(long id);
    boolean deleteOrderDetailByOrderIds(List<Long> orderid);
}
