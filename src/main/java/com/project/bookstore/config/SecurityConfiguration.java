package com.project.bookstore.config;

// import com.project.bookstore.utils.JWTAuthentication;
// import com.project.bookstore.utils.JWTFilter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
// import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("chalasa")
                .password(encodedPass().encode("1234"))
                .authorities("ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/books").permitAll()
                .antMatchers(HttpMethod.GET, "/users/**").permitAll() // need remove for Login required - Get
                .antMatchers(HttpMethod.DELETE, "/users").permitAll() // need remove - delete
                .antMatchers(HttpMethod.PUT, "/users").permitAll() // need remove - update
                .antMatchers(HttpMethod.POST, "/users").permitAll() // Login not required - create
                .antMatchers(HttpMethod.POST, "/users/orders").permitAll() // need remove - create order
                .antMatchers(HttpMethod.POST, "/login").permitAll()
                .anyRequest().authenticated();
                //.and()
                //.addFilterBefore(
                //        new JWTAuthentication(
                //                "/login",
                //                authenticationManager()
                //        ),
                //        UsernamePasswordAuthenticationFilter.class
                //);
                // .addFilterBefore(
                //     new JWTFilter(),
                //     UsernamePasswordAuthenticationFilter.class
                // );
    }

    @Bean
    public BCryptPasswordEncoder encodedPass(){
        BCryptPasswordEncoder encrypPW =  new BCryptPasswordEncoder();
        return encrypPW;
    }
   

}