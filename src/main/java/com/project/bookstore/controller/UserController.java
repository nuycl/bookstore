package com.project.bookstore.controller;

/******* BASE ******/
import org.springframework.beans.factory.annotation.Autowired;


/******* ROUTING ******/
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/******* BOOKSTORE ******/
import com.project.bookstore.model.User;
import com.project.bookstore.service.IUserService;
import com.project.bookstore.utils.HandleException;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private IUserService userService;

    // Get user list - for test
    @GetMapping("/userlist")
    @ResponseBody
    public ResponseEntity<List<User>> getUsers() {
        List<User> list = userService.getAllUsers();
        return new ResponseEntity<List<User>>(list, HttpStatus.OK);
    }

    @GetMapping("/users")
    @ResponseBody
    public ResponseEntity<User> getUser() {
        long id = userService.getUserIdFromToken("");
        User user = userService.getUserById(id);
        if (user != null) {
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
    }


     @PostMapping("/users")
     @ResponseBody
     public ResponseEntity<Void> addUser(@RequestBody User user) {
         if (userService.addUser(user)) {
            return new ResponseEntity<Void>(HttpStatus.OK);
         } 

         throw  new HandleException("Cannot add user");
     }

     @PutMapping("/users")
     @ResponseBody
     public ResponseEntity<Void> updateUser(@RequestBody User user) {
         if (userService.updateUser(user)) {
             return new ResponseEntity<Void>(HttpStatus.OK);
         } else {
             throw  new HandleException("Cannot update user");
         }
     }

     @DeleteMapping("/users")
     @ResponseBody
     public ResponseEntity<Void> deleteUser() {
         long id = userService.getUserIdFromToken("");
         if (userService.deleteUser(id)) {
             return new ResponseEntity<Void>(HttpStatus.OK);
         }
         else {
             throw  new HandleException("Delete user does not success");
         }
    }

    //-- back door API
    @GetMapping("/users/{id}")
    @ResponseBody
    public ResponseEntity<User> getUser(@PathVariable("id") Long id) {
        User user = userService.getUserById(id);
        if (user != null) {
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/users/{id}")
    @ResponseBody
    public ResponseEntity<Void> deleteUse(@PathVariable("id") Long id) {
        if (userService.deleteUser(id)) {
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        else {
            throw  new HandleException("Delete user does not success");
        }
    }


}