package com.project.bookstore.controller;

/******* ROUTING ******/
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;

@RestController
public class HomeController {

    @RequestMapping("/")
    public String index() {

        return "<h1>HOME</h1>";
    }

    @ApiOperation(value = "Sample swagger for home page", response = String.class)
    @RequestMapping(value = "/home", method = RequestMethod.POST)
    public String post() {
        return "<h1>POST TEST</h1>";
    }
}