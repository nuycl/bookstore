package com.project.bookstore.controller;

/******* BASE ******/
import com.project.bookstore.model.UserLogin;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;

/******* ROUTING ******/
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/******* BOOKSTORE ******/
import com.project.bookstore.model.User;
import com.project.bookstore.model.UserLogin;
import com.project.bookstore.service.IUserService;

@Controller
public class LoginController {
    @Autowired
    private IUserService userService;
    
    // User login
    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<Void> login(@RequestBody UserLogin login) {
        String username = login.getUsername();
        String password = login.getPassword();
        User user = userService.login(username, password);
        if (user != null) {
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
            // for UI
            // return "redirect:/login?logout";
        }
    }
}