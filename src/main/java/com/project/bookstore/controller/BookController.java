package com.project.bookstore.controller;

/******* BASE ******/
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/******* ROUTING ******/
import org.springframework.web.bind.annotation.*;

/******* BOOKSTORE ******/
import com.project.bookstore.model.Book;
import com.project.bookstore.service.IBookService;


@RestController
@RequestMapping(value="")
public class BookController {

    @Autowired
    private IBookService bookService;

    @GetMapping("/books")
    @ResponseBody
    public ResponseEntity<List<Book>> getUsers() {
        List<Book> list = bookService.getAllBooks();
        if (list == null) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        } else {
            return new ResponseEntity<List<Book>>(list, HttpStatus.OK);
        }

    }

    @GetMapping("/book/{id}")
    @ResponseBody
    public Book getBook(@PathVariable("id") Long id) {
        return bookService.getBookById(id);
    }

}
