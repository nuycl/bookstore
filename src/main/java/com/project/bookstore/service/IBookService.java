package com.project.bookstore.service;

import com.project.bookstore.model.Book;
import java.util.List;

public interface IBookService {
    List<Book> getAllBooks();
    Book getBookById(long id);
    List<Book> search(String authorName, String bookName);

}