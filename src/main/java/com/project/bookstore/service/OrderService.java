package com.project.bookstore.service;

import java.util.ArrayList;
/******* BASE ******/
import java.util.List;

import com.project.bookstore.dao.IOrderDetailDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/******* BOOKSTORE ******/
import com.project.bookstore.dao.IOrderDAO;
import com.project.bookstore.model.Order;
import com.project.bookstore.model.OrderDetail;

import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class OrderService implements IOrderService {
    @Autowired
    private IOrderDAO orderDAO;

    @Autowired
    private IOrderDetailService orderDetailService;

    @Override
    public List<Order> getOrderByUserId(long userid) {
        List<Order> orders = orderDAO.getOrderByUserId(userid);
        for (Order ord : orders) {
            List<OrderDetail> orderdetails = orderDetailService.getOrderDetailByOrderId(ord.getId());
            ord.setOrderDetails(orderdetails);
        }
        return orders;
    }

    @Override
    public List<Long> getBookidsByUserId(long userid) {
        List<Order> orders = orderDAO.getOrderByUserId(userid);
        List<Long> orderids = new ArrayList<Long>();
        List<Long> bookids = new ArrayList<Long>();
        for (Order ord : orders) {
            orderids.add(ord.getId());
        }
        
        bookids = orderDetailService.getBookidsByOrderIds(orderids);
        return bookids;
    }

    @Override
    public boolean addOrder(Order user){
        return (orderDAO.addOrder(user) != null);
    }

    @Override
    public boolean deleteUserOrder(long userid) {
        if (orderDAO.deleteUserOrder(userid)) {
            return true;
        } else {
            return false;
        }
    }

}
