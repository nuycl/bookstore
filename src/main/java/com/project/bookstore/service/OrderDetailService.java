package com.project.bookstore.service;

/******* BASE ******/
import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/******* BOOKSTORE ******/
import com.project.bookstore.dao.IOrderDetailDAO;
import com.project.bookstore.model.OrderDetail;

@Service
@Transactional
public class OrderDetailService implements IOrderDetailService {
    @Autowired
    private IOrderDetailDAO orderDetailDAO;

    @Override
    public List<Long> getBookidsByOrderIds(List<Long> orderid) {
        List<Long> bookids = orderDetailDAO.getBookidByOrderIds(orderid);
        return bookids;
    }

    @Override
    public List<OrderDetail> getOrderDetailByOrderId(Long orderid) {
        List<OrderDetail> orderDetails = orderDetailDAO.getOrderDetailByOrderId(orderid);
        return orderDetails;
    }

    @Override
    public boolean addOrderDetail(OrderDetail orderDetail){
        return (orderDetailDAO.addOrderDetail(orderDetail) != null);
    }

    @Override
    public boolean deleteOrderDetailByOrderIds(List<Long> orderId) {
        if (orderDetailDAO.deleteOrderDetailByOrderIds(orderId)) {
            return true;
        } else {
            return false;
        }
    }

}
