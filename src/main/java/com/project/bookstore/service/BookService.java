package com.project.bookstore.service;


import java.math.BigDecimal;
/******* BASE ******/
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.stereotype.Service;

// import org.springframework.http.ResponseEntity;
// import org.springframework.web.client.RestTemplate;

/******* BOOKSTORE ******/
import com.project.bookstore.model.Book;

@Service
public class BookService implements IBookService  {
    public BookService() {

    }

    // can call with promise
    private List<Book> getBookFromAPI() {
        // String uri = "https://scb-test-book-publisher.herokuapp.com/books/recommendation";
        // RestTemplate restTemplate = new RestTemplate();
        // ResponseEntity<List<Book>> response = restTemplate.exchange(
        //         uri,
        //         HttpMethod.GET,
        //         null,
        //         new ParameterizedTypeReference<List<Book>>(){});
        //  List<Book> books = response.getBody();

        List<Book> allBooks = new ArrayList<Book>(); 
        // new Book(id, author_name, book_name, price, recommended)
        allBooks.add(new Book((long)1, "Lisa Wingate", "Before We Were Yours: A Novel", BigDecimal.valueOf(340) , false));
        allBooks.add(new Book((long)2, "Barbara Davis", "When Never Comes", BigDecimal.valueOf(179), false));
        allBooks.add(new Book((long)3, "Giles Andreae, Guy Parker-Rees", "Giraffes Can't Dance", BigDecimal.valueOf(200.5), false));
        allBooks.add(new Book((long)4, "Kristin Hannah", "The Great Alone: A Novel Kristin Hannah", BigDecimal.valueOf(495), false));
        allBooks.add(new Book((long)5, "Annejet van der Zijl, Michele Hutchison", "An American Princess: The Many Lives of Allene Tew", BigDecimal.valueOf(149), false));
        allBooks.add(new Book((long)6, "Anthony William", "Medical Medium Life-Changing Foods", BigDecimal.valueOf(929.7), false));
        allBooks.add(new Book((long)7, "Gaz Oakley", "Vegan 100", BigDecimal.valueOf(897.96), false));
        allBooks.add(new Book((long)8, "Carol McCloud", "Have You Filled A Bucket Today?", BigDecimal.valueOf(290.06), false));
        allBooks.add(new Book((long)9, "Eric Carle", "The Very Hungry Caterpillar", BigDecimal.valueOf(208.5), false));
        allBooks.add(new Book((long)10, "Angie Thomas", "The Hate U Give", BigDecimal.valueOf(319.16), false));
        allBooks.add(new Book((long)11, "Kate Quinn", "The Alice Network", BigDecimal.valueOf(393.22), false));
        allBooks.add(new Book((long)12, "British Library", "Harry Potter - A History of Magic", BigDecimal.valueOf(1379.78), false));

        Collections.sort(allBooks, Book.BookNameComparator);
        return allBooks;
    }

    private List<Book> getRecommendedBookFromAPI() {
        // String uri = "https://scb-test-book-publisher.herokuapp.com/books";
        // RestTemplate restTemplate = new RestTemplate();
        // ResponseEntity<List<Book>> response = restTemplate.exchange(
        //         uri,
        //         HttpMethod.GET,
        //         null,
        //         new ParameterizedTypeReference<List<Book>>(){});
        // List<Book> books = response.getBody();

        List<Book> recommendedBooks = new ArrayList<Book>(); 
        recommendedBooks.add(new Book((long)4, "Kristin Hannah", "The Great Alone: A Novel Kristin Hannah", BigDecimal.valueOf(495), false));
        recommendedBooks.add(new Book((long)5, "Annejet van der Zijl, Michele Hutchison", "An American Princess: The Many Lives of Allene Tew", BigDecimal.valueOf(149), false));
        
        Collections.sort(recommendedBooks, Book.BookNameComparator);
        return recommendedBooks;
    }

    @Override
    public List<Book> getAllBooks()
    {
        try {

            final List<Book> bookList = new ArrayList<>();
            List<Book> recommendedList = getRecommendedBookFromAPI();
            bookList.addAll(recommendedList);

            // For check duplicate id
            List<Long> bookid = new ArrayList<>();
            for (Book book : recommendedList) {
                bookid.add(book.getId());
            }

            List<Book> allList = getBookFromAPI();
            for (Book book: allList) {
                if (bookid.indexOf(book.getId()) == -1) {
                    bookList.add(book);
                }
            }

            System.out.println(bookList.size());
            return bookList;
       }
       catch (Exception ex) {
           System.out.println("Book service got the error!");
           return null;
       }

    }

    public Book getBookById(long id) {
        return null;
    }
    
    public List<Book> search(String authorName, String bookName) {
        return null;
    }

}
