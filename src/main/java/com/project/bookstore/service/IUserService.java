package com.project.bookstore.service;

import com.project.bookstore.model.User;
import java.util.List;

public interface IUserService {
    List<User> getAllUsers();
    User getUserById(long id);
    boolean addUser(User user);
    boolean updateUser(User user);
    boolean deleteUser(long id);
    User login(String username, String password);
    long getUserIdFromToken(String token);
}