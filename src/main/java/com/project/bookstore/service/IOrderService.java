package com.project.bookstore.service;

import com.project.bookstore.model.Order;
import java.util.List;

public interface IOrderService {
    List<Long> getBookidsByUserId(long userid);
    List<Order> getOrderByUserId(long userid);
    boolean addOrder(Order order);
    boolean deleteUserOrder(long id);
}