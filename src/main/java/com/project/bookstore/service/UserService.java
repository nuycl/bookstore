package com.project.bookstore.service;

/******* BASE ******/
import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/******* BOOKSTORE ******/
import com.project.bookstore.dao.IUserDAO;
import com.project.bookstore.model.User;

@Service
@Transactional
public class UserService implements IUserService {
    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private IOrderService orderService;

    @Override
    public User getUserById(long id) {
        User user = userDAO.getUserById(id);
        return user;
    }

    @Override
    public List<User> getAllUsers(){
        return userDAO.getAllUsers();
    }

    @Override
    public boolean addUser(User user){
        if (userDAO.userExists(user.getName(), user.getSurname())) {
            System.err.println("User already register");
            return false;
        } else {
            return (userDAO.addUser(user) != null);
        }
    }

    @Override
    public boolean updateUser(User user) {
        return (userDAO.updateUser(user) != null);
    }

    @Override
    public boolean deleteUser(long id) {
        if (userDAO.deleteUser(id)) {
            orderService.deleteUserOrder(id);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public User login(String username, String password) {
        User user = userDAO.getUserLogin(username, password);
        return user;
    }

    @Override
    public long getUserIdFromToken(String token) {
        return 1;
    }
}
