package com.project.bookstore.service;

import java.util.List;
import com.project.bookstore.model.OrderDetail;

public interface IOrderDetailService {
    List<Long> getBookidsByOrderIds(List<Long> orderid);
    List<OrderDetail> getOrderDetailByOrderId(Long orderid);
    boolean addOrderDetail(OrderDetail orderDetail);
    boolean deleteOrderDetailByOrderIds(List<Long> orderid);
}