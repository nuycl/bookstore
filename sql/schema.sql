
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: 'bookstore'
--
CREATE DATABASE IF NOT EXISTS 'bookstore' DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE 'bookstore';

--
-- Table structure for table 'Orderdetails'
--

CREATE TABLE 'Orderdetails' (
  'id' bigint(20) NOT NULL,
  'orderid' bigint(20) NOT NULL,
  'bookid' bigint(20) NOT NULL,
  'count' int(11) NOT NULL,
  'price' decimal(10,2) NOT NULL,
  'deleted' tinyint(1) NOT NULL,
  'bookname' varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table 'Orders'
--

CREATE TABLE 'Orders' (
  'id' bigint(20) NOT NULL,
  'userid' bigint(20) NOT NULL,
  'total' decimal(20,2) NOT NULL,
  'orderdate' date NOT NULL,
  'deleted' tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table 'Users'
--

CREATE TABLE 'Users' (
  'id' bigint(20) NOT NULL,
  'name' char(255) NOT NULL,
  'surname' char(255) NOT NULL,
  'date_of_birth' date NOT NULL,
  'password' char(255) NOT NULL,
  'deleted' tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table 'Orderdetails'
--
ALTER TABLE 'Orderdetails'
  ADD PRIMARY KEY ('id');

--
-- Indexes for table 'Orders'
--
ALTER TABLE 'Orders'
  ADD PRIMARY KEY ('id');

--
-- Indexes for table 'Users'
--
ALTER TABLE 'Users'
  ADD PRIMARY KEY ('id');

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table 'Orderdetails'
--
ALTER TABLE 'Orderdetails'
  MODIFY 'id' bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table 'Orders'
--
ALTER TABLE 'Orders'
  MODIFY 'id' bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table 'Users'
--
ALTER TABLE 'Users'
  MODIFY 'id' bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;