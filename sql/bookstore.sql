-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Nov 18, 2018 at 05:36 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: 'bookstore'
--
CREATE DATABASE IF NOT EXISTS 'bookstore' DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE 'bookstore';

-- --------------------------------------------------------

--
-- Table structure for table 'Orderdetails'
--

CREATE TABLE 'Orderdetails' (
  'id' bigint(20) NOT NULL,
  'orderid' bigint(20) NOT NULL,
  'bookid' bigint(20) NOT NULL,
  'count' int(11) NOT NULL,
  'price' decimal(10,2) NOT NULL,
  'deleted' tinyint(1) NOT NULL,
  'bookname' varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table 'Orderdetails'
--

INSERT INTO 'Orderdetails' ('id', 'orderid', 'bookid', 'count', 'price', 'deleted', 'bookname') VALUES
(1, 1, 1, 1, '340', 0, 'Before We Were Yours: A Nove'),
(2, 2, 5, 1, '149', 0, 'An American Princess: The Many Lives of Allene Tew'),
(3, 3, 4, 1, '495', 0, 'The Great Alone: A Novel Kristin Hannah');

-- --------------------------------------------------------

--
-- Table structure for table 'Orders'
--

CREATE TABLE 'Orders' (
  'id' bigint(20) NOT NULL,
  'userid' bigint(20) NOT NULL,
  'total' decimal(20,2) NOT NULL,
  'orderdate' date NOT NULL,
  'deleted' tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table 'Orders'
--

INSERT INTO 'Orders' ('id', 'userid', 'total', 'orderdate', 'deleted') VALUES
(1, 2, '430', '2018-11-13', 0),
(2, 1, '231', '2018-10-16', 0),
(3, 1, '500', '2018-11-06', 0);

-- --------------------------------------------------------

--
-- Table structure for table 'Users'
--

CREATE TABLE 'Users' (
  'id' bigint(20) NOT NULL,
  'name' char(255) NOT NULL,
  'surname' char(255) NOT NULL,
  'date_of_birth' date NOT NULL,
  'password' char(255) NOT NULL,
  'deleted' tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table 'Users'
--

INSERT INTO 'Users' ('id', 'name', 'surname', 'date_of_birth', 'password', 'deleted') VALUES
(1, 'Chalasa', 'Lek', '2000-11-01', '$2a$10$MbGuC8WWVSdq/T9JoeDvBOPCzroC0WI7TkN63PlXp2M.Q2X0MyYZi', 0),
(2, 'Nuy', 'Small', '2019-05-22', '$2a$10$JDZCxsQ0W5uJ/IUMjP./9.1SfQs3An.XhSj/aN32liR.xxREDOeuC', 0),
(3, 'SCB', 'EAZY', '2019-05-22', '$2a$10$ScefkrLv4c5Hr3NoPBQLMe4cC.9gpTtbXXPcJcgomfmp5andZQkPK', 0),
(4, 'Cherpang', 'Captan', '2019-05-22', '$2a$10$LW34mpYGF.6MOt9YWGWXA.wr0C.Lm2IgOqEUCYv4Sdybb0lTXM21e', 0),
(5, 'Chalasa', 'Leksaravut', '2000-11-01', '3333', 0),
(6, 'Chalasa2', 'Leksaravut2', '2000-10-31', '3333', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table 'Orderdetails'
--
ALTER TABLE 'Orderdetails'
  ADD PRIMARY KEY ('id');

--
-- Indexes for table 'Orders'
--
ALTER TABLE 'Orders'
  ADD PRIMARY KEY ('id');

--
-- Indexes for table 'Users'
--
ALTER TABLE 'Users'
  ADD PRIMARY KEY ('id');

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table 'Orderdetails'
--
ALTER TABLE 'Orderdetails'
  MODIFY 'id' bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table 'Orders'
--
ALTER TABLE 'Orders'
  MODIFY 'id' bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table 'Users'
--
ALTER TABLE 'Users'
  MODIFY 'id' bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;