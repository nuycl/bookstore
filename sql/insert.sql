

--
-- Dumping data for table 'Users'
--

INSERT INTO 'Users' ('id', 'name', 'surname', 'date_of_birth', 'password', 'deleted') VALUES
(1, 'Chalasa', 'Lek', '2000-11-01', '$2a$10$MbGuC8WWVSdq/T9JoeDvBOPCzroC0WI7TkN63PlXp2M.Q2X0MyYZi', 0),
(2, 'Nuy', 'Small', '2019-05-22', '$2a$10$JDZCxsQ0W5uJ/IUMjP./9.1SfQs3An.XhSj/aN32liR.xxREDOeuC', 0),
(3, 'SCB', 'EAZY', '2019-05-22', '$2a$10$ScefkrLv4c5Hr3NoPBQLMe4cC.9gpTtbXXPcJcgomfmp5andZQkPK', 0),
(4, 'Cherpang', 'Captan', '2019-05-22', '$2a$10$LW34mpYGF.6MOt9YWGWXA.wr0C.Lm2IgOqEUCYv4Sdybb0lTXM21e', 0),
(5, 'Chalasa', 'Leksaravut', '2000-11-01', '3333', 0),
(6, 'Chalasa2', 'Leksaravut2', '2000-10-31', '3333', 0);


--
-- Dumping data for table 'Orders'
--

INSERT INTO 'Orders' ('id', 'userid', 'total', 'orderdate', 'deleted') VALUES
(1, 2, '340', '2018-11-13', 0),
(2, 1, '149', '2018-10-16', 0),
(3, 1, '644', '2018-11-06', 0);

--
-- Dumping data for table 'Orderdetails'
--

INSERT INTO 'Orderdetails' ('id', 'orderid', 'bookid', 'count', 'price', 'deleted', 'bookname') VALUES
(1, 1, 1, 1, '340', 0, 'Before We Were Yours: A Nove'),
(2, 2, 5, 1, '149', 0, 'An American Princess: The Many Lives of Allene Tew'),
(3, 3, 4, 1, '495', 0, 'The Great Alone: A Novel Kristin Hannah'),
(4, 3, 5, 1, '149', 0, 'An American Princess: The Many Lives of Allene Tew');


