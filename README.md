## SETUP ##
 - Setup database with MySQL (MAMP): Download script for generate database, tables and initial data from https://bitbucket.org/nuycl/bookstore/src/master/sql/bookstore.sql
 - Import JAVA project to your IDE and change setting for your SQL from file "application.properties" in folder "bookstore/src/main/resources"
 ![mysql setting](https://bitbucket.org/nuycl/bookstore/raw/a32e03a0a4c0d3323bb6633de9e662d686da8c29/readme/mysql.png)
 [https://bitbucket.org/nuycl/bookstore/raw/a32e03a0a4c0d3323bb6633de9e662d686da8c29/readme/mysql.png]
 - Update Gradle dependency and run on your local.
 - [Download](https://bitbucket.org/nuycl/bookstore/src/master/readme/BookStore.postman_collection.json) Postman Json file for test API from 
 

## DATABASE ##

### DATABASE DIAGRAM ###
![diagram](https://bitbucket.org/nuycl/bookstore/raw/a32e03a0a4c0d3323bb6633de9e662d686da8c29/readme/db_diagram.png)
[https://bitbucket.org/nuycl/bookstore/raw/a32e03a0a4c0d3323bb6633de9e662d686da8c29/readme/db_diagram.png]

### DATABASE INFORMATION ###
- field data type 'Long': for ID for support big data in the future.
- Soft delete: set flag 'deleted' instead of delete data from table as it should has reference for user or admin
- Add order detail for user order: As user may but many book in one order.
- Order "Total (price)" will calculate when user done the order.
- Order detail keep price and book name for refernce and book data source from another system.


## NOTE ##
 - Book List is mock data from API, as I got some problem when convert from response to list of book. The name and author gone then I just mock data for merge and sort books instead.
 - Authentication not done yet, then I allow for API and the setting in "bookstore/config/SecurityConfiguration.java"
    - need remove -> This API required login
 - AS authen did not done then user session will work for now. Please add {id} for GET and DELETE of /users
    - i.e  GET: /users/2,  DELETE: /users/2


